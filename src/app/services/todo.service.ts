import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../shared/types/todo';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  url = 'http://localhost:3000/todos';
  constructor(private api: HttpClient) {}

  getAllTasks = (): Observable<Todo[]> => {
    return this.api.get(this.url) as Observable<Todo[]>;
  };

  createTask = (task: Todo): Observable<Todo> => {
    return this.api.post(this.url, task) as Observable<Todo>;
  };

  deleteTask = (id: string | undefined): Observable<Todo> => {
    return this.api.delete(`${this.url}/${id}`) as Observable<Todo>;
  };
}
