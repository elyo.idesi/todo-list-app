import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoService } from 'src/app/services/todo.service';
import { Todo } from 'src/app/shared/types/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent {
  tasks$!: Observable<Todo[]>;

  constructor(private todoService: TodoService) {}

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks = (): void => {
    this.tasks$ = this.todoService.getAllTasks();
  };

  deleteTask = (id: string | undefined): void => {
    this.todoService.deleteTask(id).subscribe((task) => this.getTasks());
  };
}
