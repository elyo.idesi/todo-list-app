import { Component, EventEmitter, Output } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { Todo } from 'src/app/shared/types/todo';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss'],
})
export class CreateTaskComponent {
  @Output('onAddTask') onAddTask = new EventEmitter();
  task: Todo = {
    title: '',
    isComplete: false,
  };

  constructor(private todoService: TodoService) {}

  addTask = () => {
    this.todoService.createTask(this.task).subscribe((res) => {
      this.onAddTask.emit(res);
      this.task.title = '';
    });
  };
}
