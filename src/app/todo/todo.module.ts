import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import { TodoListComponent } from './todo-list/todo-list.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { FormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzListModule } from 'ng-zorro-antd/list';

@NgModule({
  declarations: [TodoListComponent, CreateTaskComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    NzButtonModule,
    NzIconModule,
    NzLayoutModule,
    FormsModule,
    NzListModule,
    NzFormModule,
    NzInputModule,
  ],
})
export class TodoModule {}
